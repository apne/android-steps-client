package gr.ait.msa.simplestepcounter.retrofit;

import gr.ait.msa.simplestepcounter.pojos.Steps;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by apne on 20-07-17.
 */

public interface ApiInterface {
    @POST("steps")
    Call<Long> reportSteps(@Body Steps steps);

}
