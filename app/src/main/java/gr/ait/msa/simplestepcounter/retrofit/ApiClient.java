package gr.ait.msa.simplestepcounter.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by apne on 20-07-17.
 */

public class ApiClient {

    public static Retrofit getClient() {
        return new Retrofit.Builder()
                .baseUrl("http://hodor.ait.gr:8080/stepsServer/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
