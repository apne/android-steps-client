package gr.ait.msa.simplestepcounter;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Date;

import gr.ait.msa.simplestepcounter.pojos.Steps;
import gr.ait.msa.simplestepcounter.retrofit.ApiClient;
import gr.ait.msa.simplestepcounter.retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private final static String TAG = MainActivity.class.getSimpleName();
    private SharedPreferences sharedPreferences;
    private String userName = "";
    private Sensor stepCounterSensor;
    private SensorManager sensorManager;
    private long steps;
    private Handler repetitionHandler;
    private Runnable repetitionRunnable;
    private ApiInterface apiInterface;

    // Widgets
    private LinearLayout userNameLayout;
    private LinearLayout stepsLayout;
    private Button setUserButton;
    private EditText userNameEditText;
    private TextView stepsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        userNameLayout = (LinearLayout) findViewById(R.id.username_layout);
        stepsLayout = (LinearLayout) findViewById(R.id.steps_layout);
        setUserButton = (Button) findViewById(R.id.set_user);
        userNameEditText = (EditText) findViewById(R.id.username);
        stepsTextView = (TextView) findViewById(R.id.steps);

        // Use the shared preferences file for the logged user
        sharedPreferences = getSharedPreferences("gr.ait.msa.goalsctivity.LOGGED_USER", MODE_PRIVATE);

        // Get user that is already logged in
        userName = sharedPreferences.getString("username", "");
        Log.i(TAG, "Stored user: " + userName);
        //mainActivityHandler = new MainActivityHandler(this);
        if (userName.equals("")) {
            // No user already logged in
            stepsLayout.setVisibility(View.GONE);
        } else {
            // User already logged in
            userNameLayout.setVisibility(View.GONE);
            initialiseSensors();
        }

        setUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName = userNameEditText.getText().toString();
                Log.i(TAG, "Entered user: " + userName);
                if (!userName.equals("")) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("username", userName);
                    editor.apply();
                    stepsLayout.setVisibility(View.VISIBLE);
                    userNameLayout.setVisibility(View.GONE);
                    initialiseSensors();
                }
            }
        });

        // Initialise repetitionHandler
        repetitionHandler = new Handler();
        // Initialise 20 sec repeating task of calculating activity metadata
        repetitionRunnable = new Runnable() {
            @Override
            public void run() {
                sendSteps();
                // Set the task for repetition after 20sec
                repetitionHandler.postDelayed(repetitionRunnable, 20000);
            }
        };
        repetitionHandler.post(repetitionRunnable);

    }

    private void sendSteps() {
        if (!userName.equals("")) {
            Steps stepsToSend = new Steps(steps, new Date(), userName);
            reportSteps(stepsToSend);
            Log.i(TAG, stepsToSend.toString());
        }
    }


    private void reportSteps(Steps stepsToSend) {
        apiInterface.reportSteps(stepsToSend).enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long> call, Response<Long> response) {
                Log.i(TAG, "Affected DB entry with ID: " + response.body());
            }

            @Override
            public void onFailure(Call<Long> call, Throwable t) {
                Log.e(TAG, t.getLocalizedMessage());
            }
        });
    }

    private void initialiseSensors() {
        // Initialise sensor manager
        sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        // Instantiate sensors and register listeners one by one
        // Instantiate step counter sensor
        stepCounterSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (stepCounterSensor == null) {
            Log.i(TAG, "No step counter sensor present...");
        }
        else {
            sensorManager.registerListener(this, stepCounterSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            steps = (long)sensorEvent.values[0];
            stepsTextView.setText(String.format("%,d", steps));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}
